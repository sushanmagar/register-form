import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  String titleText;
  String hintText;

  CustomTextField(
    this.titleText,
    this.hintText,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 8),
            child: Text(
              titleText,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          TextField(
            decoration: InputDecoration(
                hintText: hintText, contentPadding: EdgeInsets.only(left: 8)),
          ),
          SizedBox(
            height: 18,
          ),
        ],
      ),
    );
  }
}
